﻿using Models;
using SaveSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Gameplay {
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private GameGrid grid = null;
        [SerializeField] private Transform player = null;
        [SerializeField] private Transform youWonUI = null;

        private bool hasWon = false;

        // Start is called before the first frame update
        void Start()
        {

            if (FindObjectOfType<PersistentController>().loadedLevel != null)
            {
                grid.Setup(FindObjectOfType<PersistentController>().loadedLevel);

                player.position = grid.GetPlayerPosition() + new Vector3(grid.GetCellSize() / 2, grid.GetCellSize() / 2);

                CenterCameraToGrid();
            }
            else
            {
                Debug.LogError("There is no level loaded!");
            }
        }

        private void Update()
        {
            if (!hasWon)
            {
                int x = 0,
                    y = 0;

                if (Input.GetButtonDown("Horizontal"))
                {
                    x = Input.GetAxis("Horizontal") < 0 ? -1 : 1;
                }
                else if (Input.GetButtonDown("Vertical"))
                {
                    y = Input.GetAxis("Vertical") < 0 ? -1 : 1;
                }

                if (x != 0 || y != 0)
                {
                    MovePlayer(x, y);
                }
            }
        }

        private void MovePlayer(int x, int y)
        {
            bool isMoving = grid.MovePlayer(x, y, out bool isCrate);

            if (isMoving)
            {
                player.position = grid.GetPlayerPosition() + new Vector3(grid.GetCellSize() / 2, grid.GetCellSize() / 2);
            }

            if (isCrate && grid.CheckGoals())
            {
                hasWon = true;
                youWonUI.gameObject.SetActive(true);
            }
        }

        private void CenterCameraToGrid()
        {
            Vector3 gridCenter = grid.GetGridCenterPosition();
            gridCenter.z = -1;
            Camera.main.transform.position = gridCenter;
        }

        public void GoToMainMenu()
        {
            SceneManager.LoadScene("MenuScene");
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game in the editor
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
    }
}
