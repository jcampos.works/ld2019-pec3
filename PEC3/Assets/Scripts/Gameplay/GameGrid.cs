﻿using GridSystem;
using LevelEditor.Grid;
using Models;
using UnityEngine;

namespace Gameplay
{
    public class GameGrid : CustomGridComponent
    {
        private Vector2Int playerPosition;

        private void Awake()
        {
            grid = new CustomGrid<GridItem>(rows, cols, cellSize, (grid, x, y) => new GridItem(grid, x, y));
        }

        public void Setup(LevelData data)
        {
            playerPosition = new Vector2Int(data.playerPosition[0], data.playerPosition[1]);

            for (int i = 0; i < data.cratesPositions.GetLength(0); i++)
            {
                (grid as CustomGrid<GridItem>).GetValue(data.cratesPositions[i, 0], data.cratesPositions[i, 1]).SetItemType(GridItem.Type.Crate);
            }

            for (int i = 0; i < data.wallsPositions.GetLength(0); i++)
            {
                (grid as CustomGrid<GridItem>).GetValue(data.wallsPositions[i, 0], data.wallsPositions[i, 1]).SetItemType(GridItem.Type.Wall);
            }

            for (int i = 0; i < data.goalsPositions.GetLength(0); i++)
            {
                (grid as CustomGrid<GridItem>).GetValue(data.goalsPositions[i, 0], data.goalsPositions[i, 1]).SetItemType(GridItem.Type.Goal);
            }

            SetupRenderers();
        }

        public Vector3 GetPlayerPosition()
        {
            return GetWorldPosition(playerPosition.x, playerPosition.y);
        }

        public bool MovePlayer(int x, int y, out bool isCrate)
        {
            GridItem next = (grid as CustomGrid<GridItem>).GetValue(playerPosition.x + x, playerPosition.y + y);
            bool isMoving = false;

            isCrate = false;

            if (next != null)
            {
                if (next.GetItemType() == GridItem.Type.Empty || next.GetItemType() == GridItem.Type.Goal) {
                    playerPosition.x = next.x;
                    playerPosition.y = next.y;

                    isMoving = true;
                }
                else if (next.GetItemType() == GridItem.Type.Crate)
                {
                    GridItem temp = (grid as CustomGrid<GridItem>).GetValue(playerPosition.x + x * 2, playerPosition.y + y * 2);
                    if (temp.GetItemType() == GridItem.Type.Empty || temp.GetItemType() == GridItem.Type.Goal)
                    {
                        temp.SetItemType(GridItem.Type.Crate);
                        next.SetItemType(next.GetPreviousItemType());

                        playerPosition.x = next.x;
                        playerPosition.y = next.y;

                        isCrate = true;
                        isMoving = true;
                    }
                }
            }

            if (isMoving)
            {
                CallRender();
            }

            return isMoving;
        }

        public bool CheckGoals()
        {
            bool result = true;
            GridItem[,] data = (grid as CustomGrid<GridItem>).GetRawData();

            for (int i = 0; i < data.GetLength(0) && result; i++)
            {
                for (int j = 0; j < data.GetLength(1) && result; j++)
                {
                    if (data[i, j].GetItemType() == GridItem.Type.Goal)
                    {
                        result = false;
                    }
                }
            }

            return result;
        }
    }
}
