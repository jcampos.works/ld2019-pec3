﻿using GridSystem;
using LevelEditor.Grid;
using SaveSystem;

namespace Models
{
    [System.Serializable]
    public class LevelData
    {
        public string name;
        public string description;

        public int[] playerPosition;
        public int[,] cratesPositions;
        public int[,] goalsPositions;
        public int[,] wallsPositions;
    }
}
