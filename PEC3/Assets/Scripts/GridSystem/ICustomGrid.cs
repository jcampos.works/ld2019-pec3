﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridSystem
{
    public interface ICustomGrid
    {
        int GetNumRows();
        int GetNumCols();
        float GetCellSize();
        void TriggerGridValueChanged(int row, int col);
    }
}
