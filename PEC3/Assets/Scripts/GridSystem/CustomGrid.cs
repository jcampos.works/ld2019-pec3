﻿using System;

namespace GridSystem
{
    public class CustomGrid<T> : ICustomGrid
    {
        public event EventHandler<OnGridValueChangedEventArgs> OnGridValueChanged;
        public class OnGridValueChangedEventArgs : EventArgs
        {
            public int row;
            public int col;
        }

        private int rows;
        private int cols;
        private float cellSize;

        private T[,] gridArray;

        public CustomGrid(int rows, int cols, float cellSize, Func<CustomGrid<T>, int, int, T> defaultValue)
        {
            this.rows = rows;
            this.cols = cols;
            this.cellSize = cellSize;

            gridArray = new T[rows, cols];

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    gridArray[row, col] = defaultValue(this, row, col);
                }
            }
        }

        public T GetValue(int row, int col)
        {
            if (row >= 0 && col >= 0 && row < rows && col < cols)
            {
                return gridArray[row, col];
            }

            return default(T);
        }

        public void SetValue(int row, int col, T value)
        {
            if (row >= 0 && col >= 0 && row < rows && col < cols)
            {
                gridArray[row, col] = value;
                OnGridValueChanged?.Invoke(this, new OnGridValueChangedEventArgs { row = row, col = col });
            }
        }

        public void TriggerGridValueChanged(int row, int col)
        {
            OnGridValueChanged?.Invoke(this, new OnGridValueChangedEventArgs { row = row, col = col });
        }

        public int GetNumRows()
        {
            return rows;
        }

        public int GetNumCols()
        {
            return cols;
        }

        public float GetCellSize()
        {
            return cellSize;
        }

        public T[,] GetRawData()
        {
            return gridArray;
        }
    }
}
