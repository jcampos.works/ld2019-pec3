﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GridSystem
{
    public interface ICustomGridRenderer
    {
        void Setup(ICustomGrid grid);
        void OnGridValueChanged(int row, int col);
        void DoRender();
    }
}
