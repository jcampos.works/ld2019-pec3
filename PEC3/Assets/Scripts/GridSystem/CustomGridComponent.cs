﻿using System.Collections.Generic;
using UnityEngine;

namespace GridSystem
{
    public class CustomGridComponent : MonoBehaviour
    {
        protected ICustomGrid grid = null;

        protected List<ICustomGridRenderer> renderers;

        [SerializeField] protected int rows = 20;
        [SerializeField] protected int cols = 20;
        [SerializeField] protected float cellSize = 1f;

        protected void SetupRenderers()
        {
            renderers = new List<ICustomGridRenderer>();
            foreach (ICustomGridRenderer renderer in GetComponentsInChildren<ICustomGridRenderer>())
            {
                renderer.Setup(grid);
                renderers.Add(renderer);
            }
        }

        protected void CallRender()
        {
            foreach (ICustomGridRenderer renderer in GetComponentsInChildren<ICustomGridRenderer>())
            {
                renderer.DoRender();
            }
        }

        public ICustomGrid GetGrid()
        {
            return grid;
        }

        public float GetCellSize()
        {
            return cellSize;
        }

        public void GetXY(Vector3 worldPosition, out int x, out int y)
        {
            x = Mathf.FloorToInt((worldPosition - transform.position).x / cellSize);
            y = Mathf.FloorToInt((worldPosition - transform.position).y / cellSize);
        }

        public Vector3 GetWorldPosition(int row, int col)
        {
            return new Vector3(row, col) * cellSize + transform.position;
        }

        public Vector3 GetGridCenterPosition()
        {
            return GetWorldPosition(rows / 2, cols / 2) + Vector3.one * cellSize * .5f;
        }
    }
}
