﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace GridSystem
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class GridLinesRenderer : MonoBehaviour, ICustomGridRenderer
    {
        private ICustomGrid grid;

        private Mesh mesh;

        public void Setup(ICustomGrid grid)
        {
            this.grid = grid;
        }

        private void Start()
        {
            mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = mesh;

            DoRender();
        }

        public void OnGridValueChanged(int row, int col)
        {
            // NoOp
        }

        public void DoRender()
        {
            MeshUtils.CreateEmptyMeshArrays(grid.GetNumRows() * grid.GetNumCols(), out Vector3[] vertices, out Vector2[] uv, out int[] triangles);

            for (int row = 0; row < grid.GetNumRows(); row++)
            {
                for (int col = 0; col < grid.GetNumCols(); col++)
                {
                    int index = row * grid.GetNumCols() + col;
                    Vector3 quadSize = new Vector3(1, 1) * grid.GetCellSize();
                    MeshUtils.AddToMeshArrays(vertices, uv, triangles, index, GetComponentInParent<CustomGridComponent>().GetWorldPosition(row, col) + quadSize * .5f, 0f, quadSize, Vector2.zero, Vector2.one);
                }
            }

            mesh.vertices = vertices;
            mesh.uv = uv;
            mesh.triangles = triangles;
        }
    }
}
