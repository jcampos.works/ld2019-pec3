﻿using SaveSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Transform scrollListButtonPrefab = null;
    [SerializeField] private Transform scrollListContent = null;
    [SerializeField] private Transform scrollBar = null;

    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<PersistentController>().UnloadLevel();

        foreach (string file in FindObjectOfType<PersistentController>().officialLevels.Keys)
        {
            Button button = Instantiate(scrollListButtonPrefab, scrollListContent).GetComponent<Button>();
            button.GetComponentInChildren<Text>().text = "Official - " + file;
            button.onClick.AddListener(() =>
            {
                FindObjectOfType<PersistentController>().LoadLevel(file, true);
            });
        }

        foreach (string file in FindObjectOfType<PersistentController>().customLevels.Keys)
        {
            Button button = Instantiate(scrollListButtonPrefab, scrollListContent).GetComponent<Button>();
            button.GetComponentInChildren<Text>().text = file;
            button.onClick.AddListener(() =>
            {
                FindObjectOfType<PersistentController>().LoadLevel(file, false);
            });
        }

        scrollBar.GetComponent<Scrollbar>().value = 1;
    }

    public void GoToLevel()
    {
        if (FindObjectOfType<PersistentController>().HasLevelLoaded())
        {
            SceneManager.LoadScene("PlayScene");
        }
    }

    public void GoToEditor()
    {
        SceneManager.LoadScene("EditorScene");
    }
}
