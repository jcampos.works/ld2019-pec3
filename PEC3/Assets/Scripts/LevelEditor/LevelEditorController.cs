﻿using GridSystem;
using LevelEditor.Grid;
using LevelEditor.UI;
using System.Collections.Generic;
using UnityEngine;
using SaveSystem;
using Models;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

namespace LevelEditor
{
    public class LevelEditorController : MonoBehaviour
    {
        [SerializeField] private LevelEditorGrid grid = null;
        [SerializeField] private PopupController popupController = null;
        [SerializeField] private Text errorMessage = null;

        private GridItem.Type chosenType = GridItem.Type.Wall;

        private List<PaletteButton> paletteButtons;

        private LevelData loadedLevel = null;

        void Start()
        {
            paletteButtons = new List<PaletteButton>(FindObjectsOfType<PaletteButton>());
            for (int i = 1; i <= paletteButtons.Count; i++)
            {
                paletteButtons[i - 1].Setup(this, i);

                if (chosenType == (GridItem.Type)i)
                {
                    paletteButtons[i - 1].SetCheckmark(true);
                }   
            }

            grid.Setup(FindObjectOfType<PersistentController>().loadedLevel);

            CenterCameraToGrid();
        }

        private void Update()
        {
            // NoOp
        }

        public void AddItem(Vector3 worldPosition)
        {
            grid.AddItem(worldPosition, chosenType);
        }

        public void RemoveItem(Vector3 worldPosition)
        {
            grid.RemoveItem(worldPosition);
        }

        public void ChangeItemType(int index)
        {
            chosenType = (GridItem.Type)index;

            foreach (PaletteButton button in paletteButtons)
            {
                bool isActive = button.GetIndex() == index;
                button.SetCheckmark(isActive);
            }
        }

        public void SaveLevel()
        {
            if (grid.CheckLevelConfiguration(out string errors))
            {
                errorMessage.gameObject.SetActive(false); // Ensure errors are hidden

                if (loadedLevel != null)
                {
                    InternalSaveLevel(loadedLevel.name, loadedLevel.description);

                    StartCoroutine(ShowErrorMessage("LEVEL SAVED!"));
                }
                else
                {
                    Camera.main.GetComponent<CameraPan>().enabled = false;

                    popupController.ShowPopup(PopupNames.SAVE_LEVEL_POPUP);

                    popupController.OnPopupClosed += OnSaveLevelPopupClosed;
                }
            }
            else
            {
                StartCoroutine(ShowErrorMessage(errors));
            }
        }

        private void InternalSaveLevel(string name, string description)
        {
            grid.GetLevelInfo(out Vector2Int playerPosition, out Vector2Int[] crates, out Vector2Int[] goals, out Vector2Int[] walls);

            int[,] cratesPositions = new int[crates.Length, 2];
            for (int i = 0; i < crates.Length; i++)
            {
                cratesPositions[i, 0] = crates[i].x;
                cratesPositions[i, 1] = crates[i].y;
            }

            int[,] goalsPositions = new int[goals.Length, 2];
            for (int i = 0; i < goals.Length; i++)
            {
                goalsPositions[i, 0] = goals[i].x;
                goalsPositions[i, 1] = goals[i].y;
            }

            int[,] wallsPositions = new int[walls.Length, 2];
            for (int i = 0; i < walls.Length; i++)
            {
                wallsPositions[i, 0] = walls[i].x;
                wallsPositions[i, 1] = walls[i].y;
            }

            LevelData data = new LevelData()
            {
                name = name,
                description = description,
                playerPosition = new int[2] { playerPosition.x, playerPosition.y },
                cratesPositions = cratesPositions,
                goalsPositions = goalsPositions,
                wallsPositions = wallsPositions
            };

            SaveSystem.SaveSystem.Save(data, data.name, loadedLevel != null, ".bytes", "levels/");
        }

        private IEnumerator ShowErrorMessage(string errors)
        {
            errorMessage.text = errors;
            errorMessage.gameObject.SetActive(true);

            yield return new WaitForSeconds(5);

            errorMessage.gameObject.SetActive(false);
        }

        private void OnSaveLevelPopupClosed(object sender, IPopupEventArgs e)
        {
            SaveLevelPopupEventArgs args = (SaveLevelPopupEventArgs)e;
            if (args.isSave)
            {
                InternalSaveLevel(args.name, args.description);
            }

            Camera.main.GetComponent<CameraPan>().enabled = true;

            popupController.OnPopupClosed -= OnSaveLevelPopupClosed;

            StartCoroutine(ShowErrorMessage("LEVEL SAVED!"));
        }

        public void ExitEditor()
        {
            FindObjectOfType<PersistentController>().UpdateFiles();
            SceneManager.LoadScene("MenuScene");
        }

        private void CenterCameraToGrid()
        {
            Vector3 gridCenter = grid.GetGridCenterPosition();
            gridCenter.z = -1;
            Camera.main.transform.position = gridCenter;
        }
    }
}
