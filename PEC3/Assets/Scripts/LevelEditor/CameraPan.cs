﻿using GridSystem;
using LevelEditor.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelEditor
{
    public class CameraPan : MonoBehaviour
    {
        const bool DEBUG = true;

        [SerializeField] private CustomGridComponent grid = null;

        public float panSpeed = 10.0f;
        public float panBorderThickness = 10.0f;

        // Update is called once per frame
        void Update()
        {
            Vector3 position = transform.position;

            float horizontalMove = Input.GetAxisRaw("Horizontal") * panSpeed;
            float verticalMove = Input.GetAxisRaw("Vertical") * panSpeed;

            // Check mouse movement if keys aren't pressed
            if (!DEBUG && horizontalMove == 0f && verticalMove == 0f)
            {
                if (Input.mousePosition.y >= Screen.height - panBorderThickness)    // Screen top border
                {
                    verticalMove = panSpeed;
                }
                else if (Input.mousePosition.y <= panBorderThickness)               // Screen bottom border
                {
                    verticalMove = -panSpeed;
                }

                if (Input.mousePosition.x >= Screen.width - panBorderThickness)     // Screen right border
                {
                    horizontalMove = panSpeed;
                }
                else if (Input.mousePosition.x <= panBorderThickness)               // Screen left border
                {
                    horizontalMove = -panSpeed;
                }
            }

            position.x = Mathf.Clamp(position.x + horizontalMove * Time.deltaTime, 0, grid.GetGrid().GetNumCols() * grid.GetGrid().GetCellSize());
            position.y = Mathf.Clamp(position.y + verticalMove * Time.deltaTime, 0, grid.GetGrid().GetNumRows() * grid.GetGrid().GetCellSize());

            transform.position = position;
        }
    }
}
