﻿using GridSystem;
using LevelEditor.Grid;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor.UI
{
    public class PaletteButton : LevelEditorButton
    {
        private GridItem.Type type = GridItem.Type.Wall;

        [SerializeField] private Sprite crateSprite = null;
        [SerializeField] private Sprite goalSprite = null;
        [SerializeField] private Sprite playerSprite = null;
        [SerializeField] private Sprite wallSprite = null;

        private Transform checkmark;
        private Transform objectSprite;

        private void Awake()
        {
            checkmark = transform.Find("Checkmark");
            objectSprite = transform.Find("ObjectSprite");

            checkmark.gameObject.SetActive(false);
        }

        public override void Setup(LevelEditorController controller, int index)
        {
            base.Setup(controller, index);

            type = (GridItem.Type)index;

            switch (type)
            {
                default:
                case GridItem.Type.Wall:
                    objectSprite.GetComponent<Image>().sprite = wallSprite;
                    break;
                case GridItem.Type.Crate:
                    objectSprite.GetComponent<Image>().sprite = crateSprite;
                    break;
                case GridItem.Type.Goal:
                    objectSprite.GetComponent<Image>().sprite = goalSprite;
                    break;
                case GridItem.Type.Player:
                    objectSprite.GetComponent<Image>().sprite = playerSprite;
                    break;
            }
        }

        public void SetCheckmark(bool status)
        {
            checkmark.gameObject.SetActive(status);
        }

        public override void OnButtonClicked()
        {
            controller.ChangeItemType(index);
        }
    }
}
