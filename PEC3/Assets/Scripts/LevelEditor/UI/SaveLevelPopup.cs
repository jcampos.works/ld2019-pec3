﻿using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor.UI
{
    public class SaveLevelPopup : MonoBehaviour, IPopup
    {
        private PopupController controller = null;

        private string id = PopupNames.SAVE_LEVEL_POPUP;

        [SerializeField] private InputField nameInput = null;
        [SerializeField] private InputField descriptionInput = null;

        public void Setup(PopupController controller)
        {
            this.controller = controller;
        }

        public string GetId()
        {
            return id;
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void ClosePopup(bool isSave)
        {
            SaveLevelPopupEventArgs args = new SaveLevelPopupEventArgs()
            {
                isSave = isSave,
                name = nameInput.text,
                description = descriptionInput.text
            };

            controller.ClosePopup(this, args);
        }
    }
}
