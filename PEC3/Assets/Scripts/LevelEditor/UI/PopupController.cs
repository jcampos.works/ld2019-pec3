﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor.UI
{
    public class PopupController : MonoBehaviour
    {
        public event EventHandler<IPopupEventArgs> OnPopupClosed;

        private Dictionary<string, IPopup> popups;

        private Image darkenBackground;

        private void Start()
        {
            darkenBackground = GetComponent<Image>();

            popups = new Dictionary<string, IPopup>();
            for (int i = 0; i < transform.childCount; i++)
            {
                IPopup popup = transform.GetChild(i).GetComponent<IPopup>();

                if (popup != null)
                {
                    popup.Setup(this);
                    popups.Add(popup.GetId(), popup);
                }
            }
        }

        public void ShowPopup(string name)
        {
            ShowDarken();
            popups[name].Show();
        }

        public void ClosePopup(object sender, IPopupEventArgs args)
        {
            ((IPopup)sender).Hide();
            HideDarken();

            OnPopupClosed?.Invoke(sender, args);
        }

        private void ShowDarken()
        {
            darkenBackground.enabled = true;
        }

        private void HideDarken()
        {
            darkenBackground.enabled = false;
        }
    }
}
