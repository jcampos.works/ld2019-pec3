﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Utils;

namespace LevelEditor.UI {
    public class InteractablePanel : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private LevelEditorController controller = null;

        public void OnPointerDown(PointerEventData e)
        {
            Vector3 worldPosition = InputUtils.GetMouseWorldPosition2D();

            if (e.button == PointerEventData.InputButton.Left)
            {
                controller.AddItem(worldPosition);
            }
            else if (e.button == PointerEventData.InputButton.Right)
            {
                controller.RemoveItem(worldPosition);
            }
        }
    }
}
