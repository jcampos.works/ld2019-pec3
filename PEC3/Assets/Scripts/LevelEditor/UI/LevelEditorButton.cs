﻿using GridSystem;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor.UI {
    public abstract class LevelEditorButton : MonoBehaviour
    {
        public abstract void OnButtonClicked();

        protected LevelEditorController controller;
        protected int index = 1;

        public virtual void Setup(LevelEditorController controller, int index)
        {
            this.controller = controller;
            this.index = index;
        }

        public int GetIndex()
        {
            return index;
        }

    }
}
