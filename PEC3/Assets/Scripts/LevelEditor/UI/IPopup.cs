﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LevelEditor.UI
{
    public interface IPopup
    {
        void Setup(PopupController controller);
        string GetId();
        void Show();
        void Hide();
    }
}
