﻿using GridSystem;
using UnityEngine;

namespace LevelEditor.Grid {

    public class GridItemsRenderer : MonoBehaviour, ICustomGridRenderer
    {
        private CustomGrid<GridItem> grid;

        [SerializeField] private Transform gridItemPrefab = null;

        [SerializeField] private Sprite wallSprite = null;
        [SerializeField] private Sprite crateSprite = null;
        [SerializeField] private Sprite goalSprite = null;
        [SerializeField] private Sprite playerSprite = null;

        private Transform[,] visualNodes;

        public void Setup(ICustomGrid grid)
        {
            this.grid = (CustomGrid<GridItem>)grid;

            visualNodes = new Transform[grid.GetNumRows(), grid.GetNumCols()];
            for (int row = 0; row < visualNodes.GetLength(0); row++)
            {
                for (int col = 0; col < visualNodes.GetLength(1); col++)
                {
                    Vector3 position = new Vector3(row, col) * grid.GetCellSize() + Vector3.one * grid.GetCellSize() * .5f;
                    visualNodes[row, col] = CreateVisualNode("VisualNode_" + (row * grid.GetNumCols() + col), position);
                }
            }

            DoRender();
        }

        private Transform CreateVisualNode(string name, Vector3 position)
        {
            Transform visualNode = Instantiate(gridItemPrefab, position, Quaternion.identity, transform);
            visualNode.name = name;

            return visualNode;
        }

        private void UpdateVisualNode(Transform visualNode, GridItem item)
        {
            GridItem.Type type = item.GetItemType();

            switch (type)
            {
                default:
                case GridItem.Type.Empty:
                    visualNode.gameObject.SetActive(false);   // Hide visual node
                    break;
                case GridItem.Type.Wall:
                    visualNode.gameObject.SetActive(true);   // Show visual node
                    visualNode.gameObject.GetComponent<SpriteRenderer>().sprite = wallSprite;
                    break;
                case GridItem.Type.Crate:
                    visualNode.gameObject.SetActive(true);   // Show visual node
                    visualNode.gameObject.GetComponent<SpriteRenderer>().sprite = crateSprite;
                    break;
                case GridItem.Type.Goal:
                    visualNode.gameObject.SetActive(true);   // Show visual node
                    visualNode.gameObject.GetComponent<SpriteRenderer>().sprite = goalSprite;
                    break;
                case GridItem.Type.Player:
                    visualNode.gameObject.SetActive(true);   // Show visual node
                    visualNode.gameObject.GetComponent<SpriteRenderer>().sprite = playerSprite;
                    break;
            }
        }

        public void OnGridValueChanged(int row, int col)
        {
            DoRender();
        }

        public void DoRender()
        {
            for (int x = 0; x < visualNodes.GetLength(0); x++)
            {
                for (int y = 0; y < visualNodes.GetLength(1); y++)
                {
                    UpdateVisualNode(visualNodes[x, y], grid.GetValue(x, y));
                }
            }
        }
    }
}
