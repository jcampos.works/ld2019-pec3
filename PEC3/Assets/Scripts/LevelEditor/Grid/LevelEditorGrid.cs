﻿using GridSystem;
using Models;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LevelEditor.Grid
{
    public class LevelEditorGrid : CustomGridComponent
    {
        private GridItem player = null;

        private int numCrates = 0;
        private int numGoals = 0;

        private void Awake()
        {
            grid = new CustomGrid<GridItem>(rows, cols, cellSize, (grid, x, y) => new GridItem(grid, x, y));
        }

        public void Setup(LevelData data)
        {
            if (data != null)
            {
                player = (grid as CustomGrid<GridItem>).GetValue(data.playerPosition[0], data.playerPosition[1]);
                player.SetItemType(GridItem.Type.Player);

                numCrates = data.cratesPositions.GetLength(0);
                for (int i = 0; i < data.cratesPositions.GetLength(0); i++)
                {
                    (grid as CustomGrid<GridItem>).GetValue(data.cratesPositions[i, 0], data.cratesPositions[i, 1]).SetItemType(GridItem.Type.Crate);
                }

                numGoals = data.goalsPositions.GetLength(0);
                for (int i = 0; i < data.goalsPositions.GetLength(0); i++)
                {
                    (grid as CustomGrid<GridItem>).GetValue(data.goalsPositions[i, 0], data.goalsPositions[i, 1]).SetItemType(GridItem.Type.Goal);
                }

                for (int i = 0; i < data.wallsPositions.GetLength(0); i++)
                {
                    (grid as CustomGrid<GridItem>).GetValue(data.wallsPositions[i, 0], data.wallsPositions[i, 1]).SetItemType(GridItem.Type.Wall);
                }
            }

            (grid as CustomGrid<GridItem>).OnGridValueChanged += Grid_OnGridValueChanged;

            SetupRenderers();
        }

        private void Grid_OnGridValueChanged(object sender, CustomGrid<GridItem>.OnGridValueChangedEventArgs e)
        {
            foreach (ICustomGridRenderer renderer in renderers)
            {
                renderer.OnGridValueChanged(e.row, e.col);
            }
        }

        public void AddItem(Vector3 worldPosition, GridItem.Type type)
        {
            GetXY(worldPosition, out int x, out int y);
            GridItem item = (grid as CustomGrid<GridItem>).GetValue(x, y);

            if (item != null)
            {
                if (item.GetItemType() == GridItem.Type.Crate)
                {
                    numCrates -= 1;
                }
                else if (item.GetItemType() == GridItem.Type.Goal)
                {
                    numGoals -= 1;
                }

                item.SetItemType(type);

                if (type == GridItem.Type.Player)
                {
                    if (player != null)
                    {
                        player.SetItemType(GridItem.Type.Empty);
                    }
                    player = item;
                }
                else if (type == GridItem.Type.Crate)
                {
                    numCrates += 1;
                }
                else if (type == GridItem.Type.Goal)
                {
                    numGoals += 1;
                }
            }
        }

        public void RemoveItem(Vector3 worldPosition)
        {
            GetXY(worldPosition, out int x, out int y);
            GridItem item = (grid as CustomGrid<GridItem>).GetValue(x, y);

            if (item != null)
            {
                if (item.GetItemType() == GridItem.Type.Player)
                {
                    player = null;
                }
                else if (item.GetItemType() == GridItem.Type.Crate)
                {
                    numCrates -= 1;
                }
                else if (item.GetItemType() == GridItem.Type.Goal)
                {
                    numGoals -= 1;
                }

                item.SetItemType(GridItem.Type.Empty);
            }
        }

        public bool CheckLevelConfiguration(out string errors)
        {
            bool result = (player != null && numCrates == numGoals && numCrates > 0 && numGoals > 0);

            errors = "";
            if (!result)
            {
                if (player == null)
                {
                    errors += "The level should have at least a player." + Environment.NewLine;
                }

                if (numCrates == 0 || numGoals == 0)
                {
                    errors += "The level should have at least one crate and one goal." + Environment.NewLine;
                }

                if (numCrates != numGoals)
                {
                    errors += "The level should have the same amount of crates and goals." + Environment.NewLine;
                }
            }

            return result;
        }

        public void GetLevelInfo(out Vector2Int playerPosition, out Vector2Int[] crates, out Vector2Int[] goals, out Vector2Int[] walls)
        {
            GridItem[,] data = ((CustomGrid<GridItem>)grid).GetRawData();

            playerPosition = new Vector2Int(-1, -1);
            List<Vector2Int> cratesList = new List<Vector2Int>();
            List<Vector2Int> goalsList = new List<Vector2Int>();
            List<Vector2Int> wallsList = new List<Vector2Int>();

            for (int row = 0; row < data.GetLength(0); row++)
            {
                for (int col = 0; col < data.GetLength(1); col++)
                {
                    if (data[row, col].GetItemType() == GridItem.Type.Player)
                    {
                        playerPosition = new Vector2Int(row, col);
                    }
                    else if (data[row, col].GetItemType() == GridItem.Type.Crate)
                    {
                        cratesList.Add(new Vector2Int(row, col));
                    }
                    else if (data[row, col].GetItemType() == GridItem.Type.Goal)
                    {
                        goalsList.Add(new Vector2Int(row, col));
                    }
                    else if (data[row, col].GetItemType() == GridItem.Type.Wall)
                    {
                        wallsList.Add(new Vector2Int(row, col));
                    }
                }
            }

            crates = cratesList.ToArray();
            goals = goalsList.ToArray();
            walls = wallsList.ToArray();
        }
    }
}
