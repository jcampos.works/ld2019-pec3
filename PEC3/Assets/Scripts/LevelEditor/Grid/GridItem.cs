﻿using GridSystem;

namespace LevelEditor.Grid
{
    public class GridItem
    {
        public enum Type
        {
            Empty,
            Wall,
            Crate,
            Goal,
            Player
        }

        private CustomGrid<GridItem> grid;
        public int x { get; private set; }
        public int y { get; private set; }
        private Type type;
        private Type previousType = Type.Empty;

        public GridItem(CustomGrid<GridItem> grid, int x, int y)
        {
            this.grid = grid;
            this.x = x;
            this.y = y;
            type = Type.Empty;
        }

        public Type GetItemType()
        {
            return type;
        }

        public Type GetPreviousItemType()
        {
            return previousType;
        }

        public void SetItemType(Type type)
        {
            previousType = this.type;
            this.type = type;
            grid.TriggerGridValueChanged(x, y);
        }

        public override string ToString()
        {
            return type.ToString();
        }
    }
}
