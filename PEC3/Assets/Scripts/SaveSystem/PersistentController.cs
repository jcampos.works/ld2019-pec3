﻿using Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SaveSystem
{
    public class PersistentController : MonoBehaviour
    {

        [HideInInspector] public Dictionary<string, LevelData> officialLevels { get; private set; }
        [HideInInspector] public Dictionary<string, Func<LevelData>> customLevels { get; private set; }

        [HideInInspector] public LevelData loadedLevel { get; private set; }

        // Start is called before the first frame update
        void Start()
        {
            DontDestroyOnLoad(gameObject);  // Avoids destroying the scene after changing the scene

            officialLevels = new Dictionary<string, LevelData>();
            customLevels = new Dictionary<string, Func<LevelData>>();

            BinaryFormatter formatter = new BinaryFormatter();

            TextAsset[] filesFromResources = Resources.LoadAll<TextAsset>("Levels");
            foreach (TextAsset file in filesFromResources)
            {
                using (MemoryStream stream = new MemoryStream(file.bytes))
                {
                    officialLevels.Add(file.name, formatter.Deserialize(stream) as LevelData);
                }
            }

            UpdateFiles();

            SceneManager.LoadScene("MenuScene");
        }

        public bool HasLevelLoaded()
        {
            return loadedLevel != null;
        }

        public void LoadLevel(string name, bool isOfficial)
        {
            if (isOfficial && officialLevels.TryGetValue(name, out LevelData level))
            {
                loadedLevel = level;
            }
            else if (customLevels.TryGetValue(name, out Func<LevelData> loader))
            {
                loadedLevel = loader.Invoke();
            }
            else
            {
                Debug.LogError("Level with name " + name + " does not exist!");
            }
        }

        public void UnloadLevel()
        {
            loadedLevel = null;
        }

        public void UpdateFiles()
        {
            customLevels.Clear();

            string[] filesFromPersistent = SaveSystem.GetFiles("levels/");
            foreach (string file in filesFromPersistent)
            {
                customLevels.Add(file, () =>
                {
                    return SaveSystem.Load<LevelData>(file, "levels/", ".bytes");
                });
            }
        }
    }
}
