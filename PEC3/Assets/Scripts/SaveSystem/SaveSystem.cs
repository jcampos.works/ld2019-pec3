﻿using Models;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace SaveSystem
{
    public static class SaveSystem
    {
        public static void Save<T>(T data, string filename, bool overwrite = false, string extension = ".bytes", string directory = null)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/" + directory + filename + extension;

            // Create directory if it does not exist
            Directory.CreateDirectory(Path.GetDirectoryName(path));

            if (File.Exists(path) && !overwrite)
            {
                path = Application.persistentDataPath + "/" + directory + filename + DateTime.Now.ToString("_yyyy-M-dd_h-mm") + extension;
            }

            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                formatter.Serialize(stream, data);
            }
        }

        public static T Load<T>(string filename, string directory = null, string extension = ".bytes")
        {
            T data = default(T);
            string path = Application.persistentDataPath + "/" + directory + filename + extension;

            // Create directory if it does not exist
            Directory.CreateDirectory(Path.GetDirectoryName(path));

            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                
                using(FileStream stream = new FileStream(path, FileMode.Open))
                {
                    data = (T)formatter.Deserialize(stream);
                }
            }
            else
            {
                Debug.LogError("File " + filename + extension + " not found!");
            }

            return data;
        }

        public static string[] GetFiles(string directory = null, string extension = ".bytes")
        {
            // Create directory if it does not exist
            Directory.CreateDirectory(Path.GetDirectoryName(Application.persistentDataPath + "/" + directory));

            string[] paths = Directory.GetFiles(Application.persistentDataPath + "/" + directory, "*" + extension);

            string[] files = new string[paths.Length];
            for (int i = 0; i < paths.Length; i++)
            {
                files[i] = paths[i].Substring(paths[i].LastIndexOf('/') + 1, paths[i].LastIndexOf('.') - paths[i].LastIndexOf('/') - 1);
            }

            return files;
        }
    }
}
