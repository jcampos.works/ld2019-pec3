﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public class InputUtils
    {
        public static Vector3 GetMouseWorldPosition2D()
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            position.z = 0f;
            return position;
        }
    }
}
